from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /polls/
    # url(r'^$', views.index, name='index'),

    url(r'^$', views.IndexView.as_view(),name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^$(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    # ex: /polls/5/
    # ^ is start line
    # $ is end line
    # (?P<question_id>regex) this is named capturing , capture value into question_id by regex
    # this regex capture any number 0-9 with more than one letters

    # url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    #
    # url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),

    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote')
]