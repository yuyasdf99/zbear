import csv
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm
from sklearn.naive_bayes import GaussianNB

# from .models import TrainingSet

question_text = []
answer_type = []
with open('static/questionProcessing/training_question.csv','rb') as csv_file:
    spamReader = csv.reader(csv_file, delimiter=',', quotechar='|')
    for row in spamReader:
        print row[0] +','+ row[1]
        # training_set = TrainingSet(question=row[0],answer_type=row[1])
        # print(training_set)
        question_text.append(row[0])
        answer_type.append(row[1])

vectorizer = CountVectorizer(min_df=1)
analyze = vectorizer.build_analyzer()

x = vectorizer.fit_transform(question_text)
print x.toarray()
print vectorizer.get_feature_names()
# bigram_vectorizer = CountVectorizer(ngram_range=(1,2), token_pattern=r'\b\w+\b',min_df=1)
# analyze = bigram_vectorizer.build_analyzer()
# should to do

test_text = 'What the fuck'
predict_text = vectorizer.transform([test_text]).toarray()

clf = svm.SVC(decision_function_shape='ovo')
clf.fit(x,answer_type)
print 'SVM prediction'
print predict_text
print clf.predict(predict_text)

gnbClassification = GaussianNB()
gnbClassification.fit(x.toarray(),answer_type)
print 'Gaussian NB'
print predict_text
print gnbClassification.predict(predict_text)