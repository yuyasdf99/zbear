import csv

from django.test import TestCase

# Create your tests here.
from questionProcessing.models import TrainingSet


class TrainingSetTests(TestCase):

    def import_test(self):
        with open('static/questionProcessing/training_question.csv','rb') as csv_file:
            spamReader = csv.reader(csv_file, delimiter=',', quotechar='|')
            for row in spamReader:
                # print row[0]
                training_set = TrainingSet(question=row[0],answer_type=row[1])
                print(training_set)
