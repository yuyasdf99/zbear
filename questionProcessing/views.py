from django.shortcuts import render
from .models import TrainingSet
from django.views import generic
# Create your views here.

class IndexView(generic.ListView):
    template_name = 'questionProcessing/index.html'
    model = TrainingSet