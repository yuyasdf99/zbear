from django.contrib import admin
from .models import TrainingSet

# Register your models here.

# class TrainingSetAdmin(admin.ModelAdmin):
#
#     fieldsets = []


admin.site.register(TrainingSet)