from django.db import models

# Create your models here.


class TrainingSet(models.Model):

    question = models.CharField(max_length=200)
    answer_type = models.CharField(max_length=50)

    def __unicode__(self):
        return self.question